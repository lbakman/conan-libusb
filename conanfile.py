from conans import ConanFile
from conans.tools import download, unzip, replace_in_file
import os
import shutil
from conans import CMake, ConfigureEnvironment

class LibusbConan(ConanFile):
    name = "libusb"
    version = "1.0.20"
    folder = "libusb-%s" % version
    settings = "os", "arch", "compiler", "build_type"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = '''shared=False
    fPIC=True'''
    generators = "cmake"
    url = "http://ssgdjira4:7990/scm/third_party/poco-conan.git"
    license = "LGLP"

    def config(self):
        del self.settings.compiler.libcxx

    def source(self):
        zip_name = "%s.tar.bz2" % self.folder
        download("https://sourceforge.net/projects/libusb/files/libusb-1.0/%s/%s" % (self.folder, zip_name), zip_name)
        self.output.error(zip_name)
        self.untarbz2(zip_name)

    def untarbz2(self, filename, destination="."):
        import tarfile
        with tarfile.TarFile.open(filename, 'r:bz2') as tarredgzippedFile:
            tarredgzippedFile.extractall(destination)

    def build(self):
        if self.settings.os == "Windows":
            self.output.error("Windows not supported yet")
        else:
            self.build_with_make()

    def build_with_make(self):
        env = ConfigureEnvironment(self.deps_cpp_info, self.settings)
        if self.options.fPIC:
            env_line = env.command_line.replace('CFLAGS="', 'CFLAGS="-fPIC ')
        else:
            env_line = env.command_line

        custom_vars = ""

        self.run("cd %s" % self.folder)

        self.output.warn(env_line)

        #if self.settings.os == "Macos": # Fix rpath, we want empty rpaths, just pointing to lib file
        #    old_str = "-install_name \$rpath/"
        #    new_str = "-install_name "
        #    replace_in_file("%s/builds/unix/configure" % self.folder, old_str, new_str)

        #libpng_libs = 'LIBPNG_LIBS=%s'

        configure_command = 'cd %s && %s ./configure --with-harfbuzz=no %s' % (self.folder, env_line, custom_vars)
        self.output.warn("Configure with: %s" % configure_command)
        self.run(configure_command)
        self.run("cd %s && %s make" % (self.folder, env_line))

    def package(self):
        """ Define your conan structure: headers, libs and data. After building your
            project, this method is called to create a defined structure:
        """
        self.copy(pattern="libusb.h", dst="include", src="%s/libusb" % self.folder, keep_path=True)

        # UNIX
        if not self.options.shared:
            self.copy(pattern="*.a", dst="lib", src="%s" % self.folder, keep_path=False)
        else:
            self.copy(pattern="*.so*", dst="lib", src="%s" % self.folder, keep_path=False)
            self.copy(pattern="*.dylib*", dst="lib", src="%s" % self.folder, keep_path=False)

    def package_info(self):

        self.cpp_info.libs = ["libusb-1.0"]
